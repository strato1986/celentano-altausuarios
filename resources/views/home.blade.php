@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenido</div>

                <div class="panel-body">
                    Haga click <a href="{{ url('/cliente') }}">aqui</a> para registrarse en nuestro sistema.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
