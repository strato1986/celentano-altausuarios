<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cliente.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    protected function do_curl_request($url, $params=array(), $token=null) {
      if ($token != null) {
        $url = "$url&token=$token";
      }

      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/apicookie.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/apicookie.txt');

      $params_string = '';
      if (is_array($params) && count($params)) {
        foreach($params as $key=>$value) {
          $params_string .= $key.'='.$value.'&';
        }
        rtrim($params_string, '&');

        curl_setopt($ch,CURLOPT_POST, count($params));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $params_string);
      }

      //execute post
      $result = curl_exec($ch);

      //close connection
      curl_close($ch);

      return $result;
    }


    protected function opencart_login() {
      // set up params
      $url = 'http://celentano.app/index.php?route=api/login';

      $fields = array(
        'username' => 'api_user',
        'key' => '4QkPdlOD66zCH442KpxBL5OdseMGUmXmGY83R3MSC2EnRn4ydLlr3Y8r4IxrsD41p1ssj2aZu2IM57y1shUHZRabr5OjJ5vpqU8idUcK0ymXDdqko70VDC86A3deuSvpaQxFc3Jwsp1GOCWhoF5vQfKFHu4Q6cuZoV6P4o6TJmgPkQAsjDlfyDUyjgcGAQQPX0OMs8gOsMbcgtFaOiEUuuRYviMhNHWkcKoLGEWPdQd7iSfChuM7COc0AEchPbfm',
      );

      $json = $this->do_curl_request($url, $fields);
      $data = json_decode($json);

      return $data;
    }

    protected function opencart_create_customer(array $data, $token) {
      // set up params
      $url = "http://celentano.app/index.php?route=api/createcustomer";
      $fields = array(
        'firstname' => $data['firstname'],
        'lastname' => $data['lastname'],
        'telephone' => $data['telephone'],
        'customer_group_id' => 2,
        'password' => $data['password'],
        //'lastname' => 'Soni',
        'email' => $data['email'],
        'address_1' => $data['address_1'],
        'address_2' => '',
        'city' => $data['city'],
        'postcode' => '9410',
        'country_id' => 10,
        'zone_id' => 178
      );

      $json = $this->do_curl_request($url, $fields, $token);
      \Debugbar::info($json);
      $data = json_decode($json);
      return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      \Debugbar::info($request->all());
      $this->validate($request, [
          'firstname' => 'required|max:255',
          'lastname' => 'required|max:255',
          'address_1' => 'required|max:255',
          'city' => 'required|max:255',
          'dni' => 'required|max:10|min:8',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'required|min:6|confirmed',
          'telephone' => 'required|max:32|min:3',
      ]);

      $opencart = $this->opencart_login();
      $customer = $this->opencart_create_customer([
        'firstname' => $request->input('firstname'),
        'lastname' => $request->input('lastname'),
        'address_1' => $request->input('address_1'),
        'dni' => $request->input('dni'),
        'password' => $request->input('password'),
        'email' => $request->input('email'),
        'telephone' => $request->input('telephone'),
        'city' => $request->input('city')
      ], $opencart->token);

      \Flash::success('Se ha registrado correctamente.');

      return view('cliente.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
